(function ($) {
Drupal.behaviors.sanbCommon = {
  attach: function (context, settings) {
    $('.view-calendar .calendar-tid').each( function() {
      var tid = $(this).attr('calendar-tid');
      $(this).closest('.calendar').addClass('calendar-tid-' + tid + ' calendar-tid').css('cssText', 'background-color: ' + $(this).attr('background-color') + '!important;');
    });

    if (Drupal.settings.calendarColors != undefined) {
      $.each(Drupal.settings.calendarColors, function(index, value) {
        $('.views-exposed-form .form-item-edit-field-event-calendar-tid-' + index).css('background-color', value);
      });
    }

    $(window).resize( function() {
      console.log($(window).width());
    });

    $('.navbar .dropdown').hover(function() {
      if ($(window).width() > 750) {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn('fast');
      }
    }, function() {
      if ($(window).width() > 750) {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(500).fadeOut();
      }
    });
  }
}
})(jQuery);
