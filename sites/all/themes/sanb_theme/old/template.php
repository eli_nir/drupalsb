<?php


function sanb_theme_preprocess_page(&$vars) {
  if (!empty($vars['node'])) {
    if ($vars['node']->type == 'user_profile') {
      drupal_set_title('Meet the ' . $vars['node']->field_profile_title[LANGUAGE_NONE][0]['safe_value']);
    }
  }
}
