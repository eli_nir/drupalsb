<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>



<!--<header id="navbar" role="banner" class="--><?php //print $navbar_classes; ?><!--">-->
<header id="navbar" role="banner" class=" container-fluid">

    <!--  <div class="--><?php //print $container_class; ?><!--  ">-->
    <!--  <div class="--><?php //print $container_class; ?><!--">-->

    <?php if (!empty($page['top'])): ?>
        <div class="top-region"><?php print render($page['top']); ?></div>
    <?php endif; ?>

    <div class="navbar-header">
        <?php if ($logo): ?>
            <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
                <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
            </a>
        <?php endif; ?>

        <?php if (!empty($site_name)): ?>
            <a class="name navbar-brand" href="<?php print $front_page; ?>"
               title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
        <?php endif; ?>

        <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"><?php print t('Toggle navigation'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        <?php endif; ?>
    </div>

    <div class="navbar ">
        <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
            <div class="navbar-collapse collapse">
                <nav role="navigation">
                    <?php if (!empty($primary_nav)): ?>
                        <?php print render($primary_nav); ?>
                    <?php endif; ?>
                    <?php if (!empty($secondary_nav)): ?>
                        <?php //print render($secondary_nav); ?>
                    <?php endif; ?>
                    <?php if (!empty($page['navigation'])): ?>
                        <?php print render($page['navigation']); ?>
                    <?php endif; ?>
                </nav>
            </div>
        <?php endif; ?>
    </div>
    <!--  </div>-->

</header>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 hidden-xs placeholder-block">

        </div>
    </div>
</div>

<div class="main-container <?php print $container_class; ?>">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <?php if (!empty($page['slide_area'])): ?>
                <div class="slide-area-region"><?php print render($page['slide_area']); ?></div>
            <?php endif; ?>
        </div>
    </div>


    <header role="banner" id="page-header">
        <?php if (!empty($site_slogan)): ?>
            <p class="lead"><?php print $site_slogan; ?></p>
        <?php endif; ?>

        <?php print render($page['header']); ?>
    </header> <!-- /#page-header -->


    <div class="row main-content">
        <div class="col-md-10 col-md-offset-1">
            <div class="row">

                <!--                <section --><?php //print $content_column_class; ?><!-- class="col-sdm-push-6">-->

                <section class="col-sm-push-3 col-sm-6">
                  <?php if (drupal_is_front_page()): ?>
                    <h3><b>NEWS & ANNOUNCEMENTS</b></h3><br/>
                  <?php endif; ?>
                    <?php if (!empty($page['highlighted'])): ?>
                        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
                    <?php endif; ?>
                    <?php if (!empty($breadcrumb)): print $breadcrumb; endif; ?>
                    <a id="main-content"></a>

                    <?php print render($title_prefix); ?>
                    <?php if (!empty($title)): ?>
                        <h1 class="page-header"><?php print $title; ?></h1>
                    <?php endif; ?>
                    <?php print render($title_suffix); ?>
                    <?php print $messages; ?>
                    <?php if (!empty($tabs)): ?>

                        <?php print render($tabs); ?>
                    <?php endif; ?>
                    <?php if (!empty($page['help'])): ?>
                        <?php print render($page['help']); ?>
                    <?php endif; ?>
                    <?php if (!empty($action_links)): ?>
                        <ul class="action-links"><?php print render($action_links); ?></ul>
                    <?php endif; ?>
                    <?php print render($page['content']); ?>
                </section>
<!--                --><?php //if (!empty($page['sidebar_first'])): ?>
                    <aside class="col-sm-3 col-sm-pull-6 events" role="complementary">
                        <?php print render($page['sidebar_first']); ?>

                      <?php if (drupal_is_front_page()) : ?>
                      <!--        facebook-->
                      <div class="fb-page" data-href="https://www.facebook.com/San-Benito-High-School-319920813350" data-tabs="timeline"
                           data-small-header="true" data-adapt-container-width="true" data-hide-cover="false"
                           data-show-facepile="false">
                          <div class="fb-xfbml-parse-ignore">
                            <blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
                          </div>
                      </div>
                      <!--        facebook-->
                      <?php endif; ?>

                    </aside>  <!-- /#sidebar-first -->
<!--                --><?php //endif; ?>

                <?php if (!empty($page['sidebar_second'])): ?>
                    <aside class="col-sm-3 accordion" role="complementary">
                        <?php print render($page['sidebar_second']); ?>
                    </aside>  <!-- /#sidebar-second -->
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>

<?php if (!empty($page['footer'])): ?>
    <footer class="footer container-fluid">
        <div class="row">
            <div class="container">
                <?php print render($page['footer']); ?>
            </div>
        </div>
    </footer>
<?php endif; ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76384470-1', 'auto');
  ga('send', 'pageview');

</script>
